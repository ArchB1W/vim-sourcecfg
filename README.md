Forked from [vim-sourcecfg](https://github.com/rpdelaney/vim-sourcecfg)

vim-sourcecfg
=============
Vim syntax highlighting the configuration files for Valve's Source
engine game clients and servers.

Legal
========
Valve, the Valve logo, Steam, and the Steam logo are trademarks and/or
registered trademarks of Valve Corporation. All other trademarks are
property of their respective owners.
